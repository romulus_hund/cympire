import React from 'react';
import style from './Step.module.scss';
import { Divider } from 'antd';

const Step = ({step}) => {
   let backGroundClass = step.status === 'done' ? 'green' : step.status ===  'in_progress' ? 'blue' : '';
    return (
        <div className={style['step-wrapper']}>
            <div className={`step ${backGroundClass}`} key={step.step_id}>
                <div>{step.step_name}</div>
                {
                    step.status === 'in_progress' && <Divider className='divider' />
                }
            </div>
        </div>
    );
};

export default Step;
