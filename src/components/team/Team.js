import React, {Fragment} from 'react';
import style from './Team.module.scss';
import Carousel from 'react-elastic-carousel';
import Step from '../step/Step';
import EmptyState from '../emptyState/EmptyState';

const Team = ({team}) => {
    return (
        <div key={team.id} className={style['team-wrapper']}>
            <div className='team-name'>Team {team.team_name}</div>
            {
                team.steps.length ?
                    <Fragment>
                        <Carousel className='steps-wrapper' pagination={false} itemsToShow={3}>
                            {
                                team.steps.map(step => (
                                    <Step key={step.step_id} step={step}/>
                                ))
                            }
                        </Carousel>
                        <div className='step-attack-wrapper'>
                            <div className='step-attack step-wrapper'>
                                Attack completed
                            </div>
                        </div>
                    </Fragment>
                    :
                    <EmptyState
                        title={'No Steps'}
                        description=""
                    />
            }
        </div>
    );
};

export default Team;
