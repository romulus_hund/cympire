import React, {useEffect} from 'react';
import style from './App.module.scss';
import PageLayout from './components/pageLayout';
import {connect} from 'react-redux';
import {fetchData} from './store/actions/teamsActions';
import {getData, isLoading} from './store/selectors';
import Spinner from './components/Spinner';
import Team from './components/team/Team';
import EmptyState from './components/emptyState/EmptyState';

const App = ({fetchData, data, isLoading}) => {

    useEffect(() => {
        fetchData();
    }, []);

    if (isLoading) return <Spinner/>;

    return (
        <div className={style['App']}>
            <header className="App-header">
                <div className='header-details'>
                    <div>{data?.campaign_name}</div>
                </div>
            </header>
            <PageLayout>
                {
                    !data?.team_instances.length ? <EmptyState title={'No Teams'} description=""/> :
                        data?.team_instances.map(team => (
                            <Team team={team} key={team.team_id}/>
                        ))
                }
            </PageLayout>
        </div>
    );

};

const mapStateToProps = state => {
    return {
        data: getData(state),
        isLoading: isLoading(state),
    };
};

function mapDispatchToProps(dispatch) {
    return {
        fetchData: () => dispatch(fetchData()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

