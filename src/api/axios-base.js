import axios from 'axios';

const apiKey = process.env.REACT_APP_API_KEY || '';
const baseUrl = process.env.REACT_APP_API_URL || 'https://i3gy725noe.execute-api.us-east-1.amazonaws.com/default/VisualizatorApi'

const instance = axios.create({
    baseURL: baseUrl,
    headers: {'x-api-key': apiKey}
  });

export default instance;
