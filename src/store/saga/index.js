import {takeEvery} from 'redux-saga/effects';
import types from '../actionsTypes';
import {fetchDataSaga} from '../saga/sagas';

export function* watchSaga() {
  yield takeEvery(types.FETCH_DATA, fetchDataSaga);
}
