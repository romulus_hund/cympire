import { runSaga } from 'redux-saga';
import * as api from '../../api';

import {
    fetchData,
    setDataToStore,
    setLoading
} from '../actions/teamsActions';

import { fetchDataSaga } from './sagas';

describe('makeDataApiRequest', () => {
    it('should call api and dispatch success action', async () => {
        const mockData = {
            data: {
                "campaign_instance_id": "fc34b214-fb9a-48aa-a4ca-ec61fc3b00ab",
                "campaign_name": "The Wizard of OS",
                "team_instances": []
            }
        };
        const fetchDataApi = jest.spyOn(api, 'fetchDataApi').mockImplementation(() => Promise.resolve({mockData, status: 200}));
        const dispatched = [];
        const result = await runSaga({
            dispatch: (action) => dispatched.push({...action, payload: mockData}),
        }, fetchDataSaga);

        expect(fetchDataApi).toHaveBeenCalledTimes(1);
        expect(dispatched[1]).toEqual(setDataToStore(mockData));
        fetchDataApi.mockClear();
    });

});
