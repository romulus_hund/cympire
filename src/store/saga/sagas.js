import { call, put } from 'redux-saga/effects';
import types from '../actionsTypes';

import {fetchDataApi} from '../../api';
import {setLoading, setDataToStore} from "../actions/teamsActions";

export function* fetchDataSaga() {
  try {
    yield put(setLoading(true));

    const result = yield call(fetchDataApi);


    // yield put(setDataToStore(result.data));
    if (result?.status === 200) {
      yield put(setDataToStore(result.data));
    }

    yield put(setLoading(false));

  } catch (error) {
    console.log('---err: ', error);
    yield put({ type: types.FETCH_DATA_FAILED, payload: error.message });
  }
}

