import { createSelector } from 'reselect';

export const isLoading = state => state?.teamsReducer?.isLoading;

export const getData = state => state?.teamsReducer?.data;

