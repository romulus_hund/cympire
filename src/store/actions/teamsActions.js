import types from '../actionsTypes';

export const fetchData = data =>{
    return{
        type: types.FETCH_DATA,
        payload: data
    };
};

export const setLoading = data => {
    return{
        type: types.SET_LOADING,
        payload: data
    };
};

export const setDataToStore = data =>{
    return{
        type: types.SET_DATA_TO_STORE,
        payload: data
    };
};


