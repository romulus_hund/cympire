import actionsTypes from '../actionsTypes';
import createReducer from '../reducers/createReducer';

const initialState = {
    data: null,
    isLoading: false,
};

const teamsReducer = createReducer(initialState, {
    [actionsTypes.SET_LOADING]: (state, {payload}) => {
        return {
            ...state,
            isLoading: payload
        };
    },
    [actionsTypes.SET_DATA_TO_STORE]: (state, {payload}) => {
        return {
            ...state,
            data: payload
        };
    }
});

export default teamsReducer;

