import teamsReducer from './teamsReducer'
import {setDataToStore, setLoading} from '../actions/teamsActions'

describe('teams', () => {
    const initialState = {
        data: null,
        isLoading: false,
    };
    const mockData = {
        "campaign_instance_id": "fc34b214-fb9a-48aa-a4ca-ec61fc3b00ab",
        "campaign_name": "The Wizard of OS",
        "team_instances": [
            {
                "steps": [
                    {
                        "status": "not_started",
                        "step_id": "af44ae59-e045-40a7-af90-2ae4b9429b40",
                        "step_name": "Access Token Manipulation"
                    },
                    {
                        "status": "not_started",
                        "step_id": "634ce8d4-9548-4222-b7cd-b5e50593db50",
                        "step_name": "Data Compressed"
                    }
                ],
                "team_id": "829c2b2b-3985-456d-bbaf-fc5aa9ef78ca",
                "team_name": "hostTsunami"
            }
        ]
    }

    it('should return the initial state', () => {
        expect(teamsReducer(initialState, {})).toEqual(initialState);
    });

    it('should set data to store', () => {
        const stateResult = {
          isLoading: false,
          data: mockData
        };
        expect(teamsReducer(initialState, setDataToStore(mockData))).toEqual(stateResult);
    });

    it('should set isLoading to true', () => {
        const stateResult = {
            isLoading: true,
            data: null
        };
        expect(teamsReducer(initialState, setLoading(true))).toEqual(stateResult);
    });
});
