const express = require('express');
const axios = require('axios');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3000;
require('dotenv').config();

app.use(express.json());

app.use(cors());

const apiKey = process.env.REACT_APP_API_KEY || '';
const baseUrl = process.env.API_URL || '';

const axiosBase = axios.create({
    baseURL: baseUrl,
    headers: {'x-api-key': apiKey}
});

app.get('/', async (req, res) => {
    try{
        const {data} = await axiosBase.get('/');
        res.send(data);
    }catch (e) {
        console.log('--e: ', e);
        res.status(400).send();
    }
});

app.listen(port, () => {console.log('server run at port 3000');});